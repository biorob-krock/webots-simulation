/*
 * File:          
 * Date:          
 * Description:   
 * Author:        
 * Modifications: 
 */

#include <ode/ode.h>
#include <plugins/physics.h>
#include <stdio.h> 
#include "physicsPluginComTypDef.h"
/*
 * Note: This plugin will become operational only after it was compiled and associated with the current world (.wbt).
 * To associate this plugin with the world follow these steps:
 *  1. In the Scene Tree, expand the "WorldInfo" node and select its "physics" field
 *  2. Then hit the [...] button at the bottom of the Scene Tree
 *  3. In the list choose the name of this plugin (same as this file without the extention)
 *  4. Then save the .wbt by hitting the "Save" button in the toolbar of the 3D view
 *  5. Then revert the simulation: the plugin should now load and execute with the current simulation
 */

dBodyID pleurobot, ts_fl, ts_fr, ts_hl, ts_hr;
const dReal *pos_fl, *pos_fr, *pos_hl, *pos_hr;
double pfl[2048][3], pfr[2048][3], phl[2048][3], phr[2048][3];
double ffl[2048][3], ffr[2048][3], fhl[2048][3], fhr[2048][3];
int footsteps_num;

void draw_force_arrow(double start_x, double start_y, double start_z, double f_x, double f_y, double f_z, double color[3])
{
  double scale = 0.04;
  double hs = 0.02; // arrow head size

  double end_x, end_y, end_z;
  double denom, unit_x, unit_y, unit_z;
  
  end_x = start_x + scale * f_x;
  end_y = start_y + scale * f_y;
  end_z = start_z + scale * f_z;
  
  denom = sqrt((end_x - start_x) * (end_x - start_x) + (end_y - start_y) * (end_y - start_y) + (end_z - start_z) * (end_z - start_z));
  unit_x = (end_x - start_x) / denom;
  unit_y = (end_y - start_y) / denom;
  unit_z = (end_z - start_z) / denom;
  
  glDisable(GL_LIGHTING);
  glLineWidth(3);
  glBegin(GL_LINES);
  glColor3f(color[0], color[1], color[2]);
  glVertex3f(start_x, start_y, start_z);
  glVertex3f(end_x, end_y, end_z);
  glVertex3f(end_x, end_y, end_z);
  glVertex3f(+hs + end_x - unit_x * hs , end_y - unit_y * hs, end_z - unit_z * hs);
  glVertex3f(end_x, end_y, end_z);
  glVertex3f(-hs + end_x - unit_x * hs , end_y - unit_y * hs, end_z - unit_z * hs);
  glEnd();
  
  return;
}

void plot_3d_curve(double p[2048][3], int size, int start, int end, double color[3])
{
  int i, cnt=0;

  
  glDisable(GL_LIGHTING);
  glLineWidth(3);
  glBegin(GL_LINE_STRIP);
  glColor3f(color[0], color[1], color[2]);
  i=start;
  while(1){
    //dWebotsConsolePrintf("%d\t",i);
    
    i++;
    cnt++;
    if(i==end+1 || cnt>=size+1)
      break;
    else if(i>=size)
      i=0;
    glVertex3f(p[i][0], p[i][1], p[i][2]);
    
    
  }
  //dWebotsConsolePrintf("\n");
  glEnd();
  
  
}

void plot_points(double p[2048][3], int size, double color[3])
{
  int i, cnt=0;

  
  glDisable(GL_LIGHTING);
  //glLineWidth(30);
  glPointSize(10.0f);
  glBegin(GL_POINTS);
    glColor3f(color[0], color[1], color[2]);
    for(i=0; i<size; i++){
      glVertex3f(p[i][0], p[i][1], p[i][2]);
    }

  glEnd();
  
  
}

void read_vector_from_file(const char * filename, double * result, int * size)
{
  double value;
  *size = 0;
  
  FILE * read_file;
  char line[880];
  read_file = fopen(filename, "rt");  
	if(read_file == NULL){
		return; 
  }
  /*
  while(fgets(line, 880, read_file) != NULL)
  {
    sscanf(line, "%lf", &value);
    result[*size] = value;
    (*size)++;  
  }*/
  while(fscanf(read_file, "%lf", &result[*size] ) != NULL)
  {
    (*size)++;  
    if(*size>=440){
      break;
    }
  }


  fclose(read_file);  
  dWebotsConsolePrintf("size: %d\n", *size);
}

void read_and_draw(const char * filename, double color[3])
{
  int size = 0;
  double draw_data[128];
  read_vector_from_file(filename, draw_data, & size);
  if(size == 0)
    return;
  
  int i;
  for(i = 0; i < size; i += 6)
  {
    draw_force_arrow(draw_data[i + 0], draw_data[i + 1], draw_data[i + 2], 
                     draw_data[i + 3], draw_data[i + 4], draw_data[i + 5], 
                     color);
  }
}

void webots_physics_init(dWorldID world, dSpaceID space, dJointGroupID contactJointGroup) 
{
  /*
   * Get ODE object from the .wbt model, e.g.
   *   dBodyID body1 = dWebotsGetBodyFromDEF("MY_ROBOT");
   *   dBodyID body2 = dWebotsGetBodyFromDEF("MY_SERVO");
   *   dGeomID geom2 = dWebotsGetGeomFromDEF("MY_SERVO");
   * If an object is not found in the .wbt world, the function returns NULL.
   * Your code should correcly handle the NULL cases because otherwise a segmentation fault will crash Webots.
   *
   * This function is also often used to add joints to the simulation, e.g.
   *   dJointID joint = dJointCreateBall(world, 0);
   *   dJointAttach(joint, body1, body2);
   *   ...
   */
  double draw_data[1028];
  int size = 0;
  int i=0;
  pleurobot = dWebotsGetBodyFromDEF("PLEUROBOT_II");
  ts_fl = dWebotsGetBodyFromDEF("TS_FL");
  ts_fr = dWebotsGetBodyFromDEF("TS_FR");
  ts_hl = dWebotsGetBodyFromDEF("TS_HL");
  ts_hr = dWebotsGetBodyFromDEF("TS_HR");

  pos_fl = dBodyGetPosition (ts_fl);
  pos_fr = dBodyGetPosition (ts_fr);
  pos_hl = dBodyGetPosition (ts_hl);
  pos_hr = dBodyGetPosition (ts_hr);



  read_vector_from_file("footsteps.txt", draw_data, & size);
  for(i=0; i<(size/8); i++){  
    ffl[i][0]=draw_data[i];
    ffl[i][2]=draw_data[i+(size/8)];
    ffl[i][1]=0;

    ffr[i][0]=draw_data[i+2*(size/8)];
    ffr[i][2]=draw_data[i+3*(size/8)];
    ffr[i][1]=0;

    fhl[i][0]=draw_data[i+4*(size/8)];
    fhl[i][2]=draw_data[i+5*(size/8)];
    fhl[i][1]=0;

    fhr[i][0]=draw_data[i+6*(size/8)];
    fhr[i][2]=draw_data[i+7*(size/8)];
    fhr[i][1]=0;
  }
  footsteps_num=size/8;

}

void webots_physics_step() 
{
  /*
   * Do here what needs to be done at every time step, e.g. add forces to bodies
   *   dBodyAddForce(body1, f[0], f[1], f[2]);
   *   ...
   */

  int size = 0, i;
  

  ///////////////////////////////////// communicate with webots  ///////////////////////////////////
  c2p_packet *e_packet;
  e_packet = (c2p_packet*)dWebotsReceive(&size); // have to do an explicit (not implicit) cast from void* to packet* !!!!!!

  p2c_packet r_packet;

  if(e_packet!=NULL)
  {
    for(i=0; i<N_SERVOS; i++)
    {
      if(e_packet->status[i]==1)
      {
        //r_packet.torques[i] = (float)(-(e_packet->torques[i]));
        //dJointAddHingeTorque(joints[i], r_packet.torques[i]); //torque-position*0.8-speed*0.3
        //dWebotsConsolePrintf("%f\n",r_packet.torques[i]);
        //dWebotsConsolePrintf("OK!\n");
      }
      else
      {
        //dWebotsConsolePrintf("%d-th muscle not enabled: status %d \n",i,e_packet->status);
      }
    }
      
    if(sizeof(r_packet)>=1024)
    {
      dWebotsConsolePrintf(" PACKET SIZE EXCEEDS BUFFER SIZE: LIMITED TO 1024 !!!!\n ");
    }
    else
    {
      dWebotsSend(1, &r_packet, sizeof(r_packet));
      
      //dWebotsConsolePrintf("%f\n",Packet_back.torq[0]);
    }
  }
  else{
    //dWebotsConsolePrintf("NO PACKET RECEIVED FROM CONTROLLER\n");
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////
}

void webots_physics_draw() 
{

   // This function can optionally be used to add OpenGL graphics to the 3D view, e.g.
   // setup draw style 
  int j, i;
  double color1[3] = {1, 1, 0};
  double color2[3] = {1, 0, 1};
  int size=200;
  static int cnt=0, start=0, end=199, calls=0;
  if(calls==0){
    for(j=0; j<size;j++)
      for(i=0; i<3; i++){
          pfl[j][i]=pos_fl[i];
          pfr[j][i]=pos_fr[i];
          phl[j][i]=pos_hl[i];
          phr[j][i]=pos_hr[i];
        }
  }
  calls++;
  for(i=0; i<3; i++){
    pfl[start][i]=pos_fl[i];
    pfr[start][i]=pos_fr[i];
    phl[start][i]=pos_hl[i];
    phr[start][i]=pos_hr[i];
  }
  plot_3d_curve(pfl, size, start, end, color1);
  plot_3d_curve(pfr, size, start, end, color1);
  plot_3d_curve(phl, size, start, end, color2);
  plot_3d_curve(phr, size, start, end, color2);


  cnt++;
  start++;
  end++;
  if(start>=size){
    start=0;
  }
  if(end>=size){
    end=0;
  }
  if(cnt>=size)
    cnt=0;


  plot_points(ffl, footsteps_num, color1);
  plot_points(ffr, footsteps_num, color1);
  plot_points(fhl, footsteps_num, color2);
  plot_points(fhr, footsteps_num, color2);

  

}

int webots_physics_collide(dGeomID g1, dGeomID g2) 
{
  /*
   * This function needs to be implemented if you want to overide Webots collision detection.
   * It must return 1 if the collision was handled and 0 otherwise. 
   * Note that contact joints should be added to the contactJointGroup, e.g.
   *   n = dCollide(g1, g2, MAX_CONTACTS, &contact[0].geom, sizeof(dContact));
   *   ...
   *   dJointCreateContact(world, contactJointGroup, &contact[i])
   *   dJointAttach(contactJoint, body1, body2);
   *   ...
   */
  return 0;
}

void webots_physics_cleanup() 
{
  /*
   * Here you need to free any memory you allocated in above, close files, etc.
   * You do not need to free any ODE object, they will be freed by Webots.
   */
}
