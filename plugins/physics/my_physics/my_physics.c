/*
 * File:          
 * Date:          
 * Description:   
 * Author:        
 * Modifications: 
 */

#include <ode/ode.h>
#include <plugins/physics.h>
#include <stdio.h> 

/*
 * Note: This plugin will become operational only after it was compiled and associated with the current world (.wbt).
 * To associate this plugin with the world follow these steps:
 *  1. In the Scene Tree, expand the "WorldInfo" node and select its "physics" field
 *  2. Then hit the [...] button at the bottom of the Scene Tree
 *  3. In the list choose the name of this plugin (same as this file without the extention)
 *  4. Then save the .wbt by hitting the "Save" button in the toolbar of the 3D view
 *  5. Then revert the simulation: the plugin should now load and execute with the current simulation
 */

dBodyID ghouldog;
 
void draw_force_arrow(double start_x, double start_y, double start_z, 
  double f_x, double f_y, double f_z,
  double color[3])
{
  double scale = 0.04;
  double hs = 0.02; // arrow head size

  double end_x, end_y, end_z;
  double denom, unit_x, unit_y, unit_z;
  
  end_x = start_x + scale * f_x;
  end_y = start_y + scale * f_y;
  end_z = start_z + scale * f_z;
  
  denom = sqrt((end_x - start_x) * (end_x - start_x) + (end_y - start_y) * (end_y - start_y) + (end_z - start_z) * (end_z - start_z));
  unit_x = (end_x - start_x) / denom;
  unit_y = (end_y - start_y) / denom;
  unit_z = (end_z - start_z) / denom;
  
  glDisable(GL_LIGHTING);
  glLineWidth(3);
  glBegin(GL_LINES);
  glColor3f(color[0], color[1], color[2]);
  glVertex3f(start_x, start_y, start_z);
  glVertex3f(end_x, end_y, end_z);
  glVertex3f(end_x, end_y, end_z);
  glVertex3f(+hs + end_x - unit_x * hs , end_y - unit_y * hs, end_z - unit_z * hs);
  glVertex3f(end_x, end_y, end_z);
  glVertex3f(-hs + end_x - unit_x * hs , end_y - unit_y * hs, end_z - unit_z * hs);
  glEnd();
  
  return;
}

void read_vector_from_file(const char * filename, double * result, int * size)
{
  double value;
  *size = 0;
  
  FILE * read_file;
  char line[80];
  read_file = fopen(filename, "rt");  
	if(read_file == NULL)
    return; 

  while(fgets(line, 80, read_file) != NULL)
  {
    sscanf(line, "%lf", &value);
    result[*size] = value;
    (*size)++;  
  }
  
  fclose(read_file);  
}

void read_and_draw(const char * filename, double color[3])
{
  int size = 0;
  double draw_data[128];
  read_vector_from_file(filename, draw_data, & size);
  if(size == 0)
    return;
  
  int i;
  for(i = 0; i < size; i += 6)
  {
    draw_force_arrow(draw_data[i + 0], draw_data[i + 1], draw_data[i + 2], 
                     draw_data[i + 3], draw_data[i + 4], draw_data[i + 5], 
                     color);
  }
}

void webots_physics_init(dWorldID world, dSpaceID space, dJointGroupID contactJointGroup) {
  /*
   * Get ODE object from the .wbt model, e.g.
   *   dBodyID body1 = dWebotsGetBodyFromDEF("MY_ROBOT");
   *   dBodyID body2 = dWebotsGetBodyFromDEF("MY_SERVO");
   *   dGeomID geom2 = dWebotsGetGeomFromDEF("MY_SERVO");
   * If an object is not found in the .wbt world, the function returns NULL.
   * Your code should correcly handle the NULL cases because otherwise a segmentation fault will crash Webots.
   *
   * This function is also often used to add joints to the simulation, e.g.
   *   dJointID joint = dJointCreateBall(world, 0);
   *   dJointAttach(joint, body1, body2);
   *   ...
   */
   
     // get body of the robot part
  //dBodyID body = dWebotsGetBodyFromDEF("RIGHT_FRONT_YAW");
dBodyID body = dWebotsGetBodyFromDEF("S_SPINE1");
  // the joint group ID is 0 to allocate the joint normally
  dJointID joint = dJointCreateFixed(world, 0);

  // attach robot part to the static environment: 0
  dJointAttach(joint, body, 0);

  // fix now: remember current position and rotation
  dJointSetFixed(joint);
  
  
  ghouldog = dWebotsGetBodyFromDEF("force_point");

}

void webots_physics_step() {
  /*
   * Do here what needs to be done at every time step, e.g. add forces to bodies
   *   dBodyAddForce(body1, f[0], f[1], f[2]);
   *   ...
   */
  
  int size = 0;
  double draw_data[128];
  read_vector_from_file("/home/thorvat/BioRobLab/WEBOTS/pleurobot 2 - online/plugins/physics/my_physics/ghouldog_ext_f.txt", draw_data, & size);

  if(size == 0)
    return;
  
  int i;
  for(i = 0; i <size; i += 6)
  {
    dBodyAddForce(ghouldog, draw_data[i + 3], draw_data[i + 4], draw_data[i + 5]);
    //dBodyAddForce(ghouldog, 100,100,100);
  }
  
}

void webots_physics_draw() {

   // This function can optionally be used to add OpenGL graphics to the 3D view, e.g.
   // setup draw style 

  double color[3] = {1, 1, 0};
  read_and_draw("/home/thorvat/BioRobLab/WEBOTS/pleurobot 2 - online/plugins/physics/my_physics/ghouldog.txt", color);
  
  color[1] = 0;
  read_and_draw("/home/thorvat/BioRobLab/WEBOTS/pleurobot 2 - online/plugins/physics/my_physics/ghouldog_ext_f.txt", color);
}

int webots_physics_collide(dGeomID g1, dGeomID g2) {
  /*
   * This function needs to be implemented if you want to overide Webots collision detection.
   * It must return 1 if the collision was handled and 0 otherwise. 
   * Note that contact joints should be added to the contactJointGroup, e.g.
   *   n = dCollide(g1, g2, MAX_CONTACTS, &contact[0].geom, sizeof(dContact));
   *   ...
   *   dJointCreateContact(world, contactJointGroup, &contact[i])
   *   dJointAttach(contactJoint, body1, body2);
   *   ...
   */
  return 0;
}

void webots_physics_cleanup() {
  /*
   * Here you need to free any memory you allocated in above, close files, etc.
   * You do not need to free any ODE object, they will be freed by Webots.
   */
}
