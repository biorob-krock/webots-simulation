#include <stdlib.h>
#include <ode/ode.h>
#include <plugins/physics.h>

#include "hydro.h"
#include "hydro_boxes.h"

#define HEAD "ROBOT"
#define TAIL "SP10"
#define PASSIVE1 "PS_SPINE1"
#define PASSIVE2 "PS_SPINE2"

enum {X, Y, Z};

#define GRAVITY              9.81
#define WATER_LEVEL          1.0
#define WATER_DENSITY        1000.0
#define ARCHIMEDE_FACTOR     2.0//1.8      /* correction for tuning the Archimede force */

#define DRAW_SCALE_BOX_AXES           0
#define DRAW_SCALE_HYDRO_FORCES       0
#define DRAW_SCALE_HYDRO_TORQUES      0
#define DRAW_SCALE_ARCHIMEDE_FORCES   0
#define DRAW_SCALE_EXTREME_CORNERS    0

/* Matrix for change of basis between Webots solid and hydro frames.
 * Each column is the new basis vector expressed in the old basis.
 * Note that inv(M) = Mt.
 * To transform matrix A from old to new base: A' = Mt A M.
 * To transform vector x from old to new bas: x' = Mt x. */
static Matrix3 hydro_to_webots[NUM_BOXES];
static Matrix3 webots_to_hydro[NUM_BOXES];

static dBodyID bodies[NUM_BOXES];
static dJointID joint;
static HydroSegment segments[NUM_BOXES];
static double archimede_force[NUM_BOXES];
static Vector6 hydro_wrench[NUM_BOXES];
static HydroModel hydro;
static dWorldID world;
static dSpaceID space;
static dJointGroupID contact_joint_group;

static int enable_hydro = 1;

static int head_index;
static int tail_index;

static Vector3 top_corners[NUM_BOXES];
static Vector3 bottom_corners[NUM_BOXES];

static Vector3 colors[] = {{1, 0, 0},
                           {0, 1, 0},
                           {0, 0, 1},
                           {0, 1, 1},
                           {1, 0, 1},
                           {1, 1, 0},
                           {0, 0, 0},
                           {1, 1, 1},
                           {0, 0.5, 1}};

enum {
	RED,
	GREEN,
	BLUE,
	CYAN,
	MAGENTA,
	YELLOW,
	BLACK,
	WHITE,
	WATER
};

static double
immersed_fraction (double h0, double h1)
{
	if (h1 - h0 < 0.00001)
	{
		return 0.5;
	}
	else
	{
		double fraction = (WATER_LEVEL - h0) / (h1 - h0);
		return fraction < 0 ? 0 : (fraction > 1 ? 1 : fraction);
	}
}

static void
draw_p1_p2 (Vector p1, Vector p2)
{
	glBegin(GL_LINES);
	glVertex3f(p1[0], p1[1], p1[2]);
	glVertex3f(p2[0], p2[1], p2[2]);
	glEnd();
}

static void
draw_p1_v (Vector p1, Vector v)
{
	Vector3 p2;

	draw_p1_p2 (p1, vector3_sum(p1, v, p2)); // p2 = p1+v
}

static void
draw_arrow (Vector p1,
            Vector v,
            Vector z)
{
	Vector3 p2;
	
	vector3_sum (p1, v, p2);

	/* "horizontal" vector perpendicular to v */
	Vector3 axis;
	vector3_cross_vector (v, z, axis);

	/* if v and z are colinear, then z doesn't properly define the plane for the arrow head
	 * and we take an arbitrary plane instead*/
	if (vector3_norm (axis) < 0.00001)
	{
		Vector3 arbitrary = {-v[1], v[0], v[2]};
		vector_copy (axis, arbitrary, 3);
	}
	
	/* vector defining plane for arrow head */
	vector3_cross_vector (axis, v, axis);

	Vector3 head;
	vector3_multiply (v, -0.1, head);

	Quaternion rot;
	Vector3 head1, head2;
	quaternion_from_axis_angle (axis, M_PI / 4, rot);
	quaternion_rotation (rot, head, head1);

	quaternion_from_axis_angle (axis, -M_PI / 4, rot);
	quaternion_rotation (rot, head, head2);
	
	draw_p1_p2 (p1, p2);
	draw_p1_v (p2, head1);
	draw_p1_v (p2, head2);
}

static void
draw_box_axes (int box_index, Vector3 z)
{
	Vector3 origin;

	dBodyGetRelPointPos (bodies[box_index],
	                     boxes[box_index].translation[0],
	                     boxes[box_index].translation[1],
	                     boxes[box_index].translation[2],
	                     origin);

	int i;
	for (i = 0; i < 3; ++i)
	{
		Vector3 axis = {0, 0, 0};

		axis[i] = boxes[box_index].size[i] / 2;
		quaternion_rotation (boxes[box_index].rotation, axis, axis);
		dBodyVectorToWorld (bodies[box_index], axis[0], axis[1], axis[2], axis);
		vector3_multiply (axis, DRAW_SCALE_BOX_AXES, axis);

		glColor3dv (colors[RED + i]);
		draw_arrow (origin, axis, z);
	}
}

static void
draw_extreme_corners (int box_index, Vector3 z)
{
	Vector3 arrow;

	vector3_difference (top_corners[box_index], bottom_corners[box_index], arrow);
	vector3_multiply (arrow, DRAW_SCALE_EXTREME_CORNERS, arrow);

	glColor3dv (colors[WHITE]);
	draw_arrow (bottom_corners[box_index], arrow, z);
	
	vector3_multiply (arrow, immersed_fraction (bottom_corners[box_index][Y], top_corners[box_index][Y]), arrow);

	glColor3dv (colors[WATER]);
	draw_p1_v (bottom_corners[box_index], arrow);
}

static void
draw_hydro_forces (int box_index, Vector3 z)
{
	double const *position = dBodyGetPosition (bodies[box_index]);
	double *wrench = hydro_wrench[box_index];
	Vector3 force;

	dBodyVectorToWorld (bodies[box_index], wrench[0], wrench[1], wrench[2], force);
	vector3_multiply (force, DRAW_SCALE_HYDRO_FORCES, force);

	glColor3dv (colors[CYAN]);
	draw_arrow ((Vector) position, force, z);
}

static void
draw_hydro_torques (int box_index, Vector3 z)
{
	double const *position = dBodyGetPosition (bodies[box_index]);
	double *wrench = hydro_wrench[box_index];
	Vector3 torque;

	dBodyVectorToWorld (bodies[box_index], wrench[3], wrench[4], wrench[5], torque);
	vector3_multiply (torque, DRAW_SCALE_HYDRO_TORQUES, torque);

	glColor3dv (colors[MAGENTA]);
	draw_arrow ((Vector) position, torque, z);
}

static void
draw_archimede_forces (int box_index, Vector3 z)
{
	double const *position = dBodyGetPosition (bodies[box_index]);
	Vector3 force = {0, archimede_force[box_index], 0};

	vector3_multiply (force, DRAW_SCALE_ARCHIMEDE_FORCES, force);

	glColor3dv (colors[YELLOW]);
	draw_arrow ((Vector) position, force, z);
}

void
webots_physics_draw ()
{
	glPushAttrib (GL_ENABLE_BIT | GL_LINE_WIDTH);
		
	glDisable (GL_LIGHTING);
	glLineWidth (2.0);

	int box_index;
	for (box_index = 0; box_index < NUM_BOXES; ++box_index)
	{
		Vector3 z;
		quaternion_rotation (boxes[box_index].rotation, z, z);
		dBodyVectorToWorld (bodies[box_index], z[0], z[1], z[2], z);

		glDisable (GL_DEPTH_TEST);
		draw_box_axes (box_index, z);
		draw_extreme_corners (box_index, z);
		draw_hydro_forces (box_index, z);
		draw_hydro_torques (box_index, z);
		draw_archimede_forces (box_index, z);
		glEnable (GL_DEPTH_TEST);
	}

	glPopAttrib ();
}

/* initializes the segment structure, adds reactive term of the hydro
 * model to the body inertia tensor */
static void
box_init (int box_index)
{
	HydroSegment *segment = &segments[box_index];
	Matrix3 I = {{1, 0, 0},
	             {0, 1, 0},
	             {0, 0, 1}};

	/* rotation * e1 is the first column of hydro_to_webots, hence first row of webots_to_hydro */
	int i;
	for (i = 0; i < 3; ++i)
	{
		quaternion_rotation (boxes[box_index].rotation, I[i], webots_to_hydro[box_index][i]);
	}

	matrix3_transpose (webots_to_hydro[box_index], hydro_to_webots[box_index]);
	
	/* Segment initialization */
	segment->index = box_index;
	segment->hydro = &hydro;
	segment->length = boxes[box_index].size[0];
	segment->width = boxes[box_index].size[1];
	segment->height = boxes[box_index].size[2];
	segment->origin_offset = segment->length / 2.0; /* in hydro's frame of reference, x coordinate of the point for which linear velocity is given
	                                                 * i.e. x coordinate of the box center */
	
	/* Get added mass from hydrodynamical model in segment->Iadd */
	hydro_segment_calculate_added_mass (segment);

#ifdef ADDED_MASS
	dMass mass0, mass;
	Matrix3 Iadded, M;
	double added_mass;
	dBodyID body = bodies[box_index];

	/* Get current mass from ODE body */
	dBodyGetMass (body, &mass0);

	/* Extract inertia matrix from segment->Iadd into Iadded */
	/* Get actual mass in added_mass */
	matrix6_get_block3 (segment->Iadd, Iadded, 3, 3);
	matrix6_get_block3 (segment->Iadd, M, 0, 0);
	added_mass = segment->Iadd[1][1];

	/* Express Iadded in ODE's coordinate system */
	matrix3_product_matrix3 (webots_to_hydro[box_index], Iadded, Iadded);
	matrix3_product_matrix3 (Iadded, hydro_to_webots[box_index], Iadded);
	
	/* Prepare new mass structure with added mass */
	dMassSetParameters (&mass,
	                    added_mass,
	                    0, 0, 0, /* correct? */
	                    Iadded[0][0], Iadded[1][1], Iadded[2][2],
	                    Iadded[0][1], Iadded[0][2], Iadded[1][2]);

	/* Add it to the current mass */
	dMassAdd (&mass, &mass0);

	/* Set the new body mass */

	dBodySetMass (body, &mass);
#endif
}
// --- passive joint---
static void
k_d_to_cfm_and_erp(double h, double k, double d, double *cfm, double *erp)
{
  *cfm = 1 / (h * k + d);
  *erp = (h * k) / (h * k + d);
}

static void
series_elastic_joint_apply(dJointID joint, double h, double k, double d)
{
	double cfm;
	double erp;

	if (!joint)
	{
		return;
	}

	k_d_to_cfm_and_erp(h, k, d, &cfm, &erp);

	dJointSetHingeParam(joint, dParamStopCFM, cfm);
	dJointSetHingeParam(joint, dParamStopERP, erp);
}

// ---------------------

void
webots_physics_init (dWorldID      w,
                     dSpaceID      s,
                     dJointGroupID j)
{
	int i;

	world = w;
	space = s;
	contact_joint_group = j;

	hydro.cf = 2;
	hydro.cd = 1.75;
	hydro.cm = 0.5;
	hydro.cp = 1;
	hydro.rho_fluid = WATER_DENSITY;
	
	for (i = 0; i < NUM_BOXES; ++i)
	{
		bodies[i] = dWebotsGetBodyFromDEF (boxes[i].solid_def);

		if (bodies[i])
		{
			box_init (i);

			if (!strcmp(boxes[i].solid_def, HEAD))
			{
				head_index = i;
			}

			if (!strcmp(boxes[i].solid_def, TAIL))
			{
				tail_index = i;
			}
      if (!strcmp(boxes[i].solid_def, PASSIVE2))
      {
        joint = dBodyGetJoint(bodies[i], 0);
        dJointSetHingeParam(joint, dParamLoStop, 0);
        dJointSetHingeParam(joint, dParamHiStop, 0);
        //dWebotsConsolePrintf("%d\n",dBodyGetNumJoints (bodies[i]));
      }
      if (!strcmp(boxes[i].solid_def, PASSIVE1))
      {
        joint = dBodyGetJoint(bodies[i], 1);
        dJointSetHingeParam(joint, dParamLoStop, 0);
        dJointSetHingeParam(joint, dParamHiStop, 0);
        //dWebotsConsolePrintf("%d\n",dBodyGetNumJoints (bodies[i]));
      }
		}
		else
		{
			dWebotsConsolePrintf ("Warning: couldn't find solid with DEF \"%s\"", boxes[i].solid_def);
		}
	}

	dWebotsConsolePrintf (enable_hydro ? "Hydrodynamics enabled\n" : "Hydrodynamics disabled\n");
}

static void
box_extreme_corners (int box_index)
{
	dBodyID body = bodies[box_index];
	double const *solid_position = dBodyGetPosition (body);
	double const *solid_rotation = dBodyGetQuaternion (body);

	Vector3 box_position;
	Quaternion box_rotation;

	quaternion_rotation ((double *) solid_rotation, boxes[box_index].translation, box_position);
	vector3_sum ((double *) solid_position, box_position, box_position);

	quaternion_product_quaternion ((double *) solid_rotation, boxes[box_index].rotation, box_rotation);

	bottom_corners[box_index][Y] = DBL_MAX;
	top_corners[box_index][Y] = DBL_MIN;

	/* Get height of lowest and highest box corners */
	int i;
	for (i = -1; i <= 1; i += 2)
	{
		int j;
		for (j = -1; j <= 1; j += 2)
		{
			int k;
			for (k = -1; k <= 1; k += 2)
			{
				Vector3 corner = {0.5 * boxes[box_index].size[X] * i,
				                  0.5 * boxes[box_index].size[Y] * j,
				                  0.5 * boxes[box_index].size[Z] * k};

				quaternion_rotation (box_rotation, corner, corner);
				vector3_sum (corner, box_position, corner);
				
				if (corner[Y] < bottom_corners[box_index][Y])
				{
					vector_copy (bottom_corners[box_index], corner, 3);
				}

				if (corner[Y] > top_corners[box_index][Y])
				{
					vector_copy (top_corners[box_index], corner, 3);
				}
			}
		}
	}
}

static void
box_add_archimede_force (HydroSegment *segment,
                         dBodyID       body,
                         int           box_index)
{
	double box_volume = boxes[box_index].size[0] * boxes[box_index].size[1] * boxes[box_index].size[2];
	double fraction = immersed_fraction (bottom_corners[box_index][Y], top_corners[box_index][Y]);

	archimede_force[box_index] = GRAVITY * WATER_DENSITY * box_volume * ARCHIMEDE_FACTOR * fraction;

#ifdef ADDED_MASS
	/* compensate for the mass added by the hydrodynamics model */
	double added_mass = segment->Iadd[1][1];
	archimede_force[box_index] += GRAVITY * added_mass;
#endif

	dBodyAddForce (body, 0, archimede_force[box_index], 0);
}

static void
box_add_hydro_forces (int box_index)
{
	dBodyID body = bodies[box_index];
	HydroSegment *segment = &segments[box_index];
	const double *linear_vel = dBodyGetLinearVel (body);
	const double *angular_vel = dBodyGetAngularVel (body);
	Vector6 segment_force, head_force, tail_force;

	box_add_archimede_force (segment, body, box_index);
	
	dBodyVectorFromWorld (body, linear_vel[0], linear_vel[1], linear_vel[2], LINEAR (segment->velocity));
	dBodyVectorFromWorld (body, angular_vel[0], angular_vel[1], angular_vel[2], ANGULAR (segment->velocity));

	Vector3 box_vel;
	vector3_cross_vector (ANGULAR (segment->velocity), boxes[box_index].translation, box_vel);
	vector3_sum (box_vel, LINEAR (segment->velocity), LINEAR (segment->velocity));

	matrix3_product_vector6 (hydro_to_webots[box_index], segment->velocity, segment->velocity);
	hydro_segment_get_segment_force (segment, segment_force);
	matrix3_product_vector6 (webots_to_hydro[box_index], segment_force, segment_force);

	if (box_index == head_index)
	{
		hydro_segment_get_head_force (segment, head_force);
		matrix3_product_vector6 (webots_to_hydro[box_index], head_force, head_force);
		vector6_sum (head_force, segment_force, segment_force);
	}

	if (box_index == tail_index)
	{
		hydro_segment_get_caudal_force (segment, tail_force);
		matrix3_product_vector6 (webots_to_hydro[box_index], tail_force, tail_force);
		vector6_sum (tail_force, segment_force, segment_force);
	}

	/* translate the wrench back to the center of mass */
	Vector3 box_to_com;
	vector3_multiply (boxes[box_index].translation, -1, box_to_com);

	/* twist transformation matrix */
	Matrix6 A, AT;
	vector3_translation_transformation_matrix (box_to_com, AT);
	matrix6_transpose (AT, A);
	matrix6_product_vector (A, segment_force, segment_force);

	double *wrench = hydro_wrench[box_index];
	double fraction = immersed_fraction (bottom_corners[box_index][Y], top_corners[box_index][Y]);
	vector_multiply (segment_force, -fraction, wrench, 6);

	dBodyAddRelForce (body, wrench[0], wrench[1], wrench[2]);
	dBodyAddRelTorque (body, wrench[3], wrench[4], wrench[5]);
}

void
webots_physics_step ()
{
	int i;

	if (!enable_hydro)
	{
		return;
	}

	for (i = 0; i < NUM_BOXES; ++i)
	{
		box_extreme_corners (i);
		box_add_hydro_forces (i);
    if (!strcmp(boxes[i].solid_def, PASSIVE2))
    {
      joint = dBodyGetJoint(bodies[i], 0);
      series_elastic_joint_apply(joint, 0.004, 2, 0.01);
    }
    if (!strcmp(boxes[i].solid_def, PASSIVE1))
    {
      joint = dBodyGetJoint(bodies[i], 1);
      series_elastic_joint_apply(joint, 0.004, 2, 0.01);
    }
	}
}

void
webots_physics_cleanup ()
{
}
