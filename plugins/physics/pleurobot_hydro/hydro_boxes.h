#ifndef __HYDRO_BOXES_H
#define __HYDRO_BOXES_H

#include "matrix.h"

typedef struct
{
	char const *solid_def;
	Vector3 size;          /* in box hydro frame */
	Vector3 translation;   /* from solid center of mass to box center, in solid frame */
	Quaternion rotation;   /* of box hydro frame, in solid frame */
} Box;

#define NUM_BOXES 26//14//26

Box boxes[NUM_BOXES];

#endif

