# System Properties of Krock in Reality and Simulation

## Krock in Webots

### Coulomb Friction

One of the most influential paramteres on the outcome of the experiments is friction. For that, there are two modified `coulombFriction` properties in the `Worldinfo` node of `webots`. Follow the proceeding steps to find these variables:

- Open the first (`Worldinfo`) node of the `webots` tree

- Select `contactProperties` and choose the first `ContactProperties` node

- `material1` is defined as "foot". Also, the variable `bounce` is set to ero. Finally, `coulombFriction` is set to `0.5`

- For the second `ContactProperties` node (which I suppose corresponds to the friction of the body on ground) `coulombFriction` is set to `0.25`

### Weight

To measure the net weight of the robot in the simulation, we sould add all the `physics` node present in the robot. The difference in speed of the `Krock` can come from weight difference of the simulation and real robot.