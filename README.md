# Krock-2 Webots simulation

For Webots itself, simply go through the [Webots installation procedure](https://cyberbotics.com/doc/guide/installation-procedure). 

## Install 

Clone this repository:
```bash
git clone https://gitlab.com/biorob-krock/webots-simulation.git
cd webots-simulation
```

Init submodules:
```bash
git submodule init
# This can take a while
git submodule update --remote
```

> If you want to update the submodules to get the latest versions after some 
> modifications were made on the upstream repository:  
> `git submodule update --remote`

The submodule is actually the [robot controller repository](https://gitlab.com/biorob-krock/krock-controller),
so the same code base for the controller is used both for the hardware robot and
the webots simulation to avoid code repetition and facilitate the maintenance. 
Only relevant parts of the code are different (e.g. `main_webots.cpp` and 
`source/robotSim.cpp` instead of`main.cpp` and `source/robot.cpp`).

You need to install the dependencies for the controller, as explained in the 
[README](https://gitlab.com/biorob-krock/krock-controller/blob/master/README.md) 
of the controller repository. Once the dependencies are correctly installed, you
can compile the controller for webots with the provided `CMakeLists.txt`:

```bash
cd controllers/krock-controller/lib
make -j8
cd ../controllers/krock-controller
mkdir build
cd build
# FOR_WEBOTS=ON to compile for Webots instead of the hardware robot
cmake .. -DFOR_WEBOTS=ON
make -j8
```

The makefile will automatically name the executable with the controller repository
folder name (i.e. `krock-controller`) and place it at the right place, as required 
by webots to correctly load it.

If `make` didn't produce any errors, you should have the executable `krock-controller`. 
It cannot be directly executed, but will be loaded in Webots. To check if it is 
working, launch webots and open one of the simulations. 

The simulation depends on ROS running, so first launch `rosecore`. 
If this step is skipped, the controller will just hang and time will not progress in the simulation; 
krock will remain floating above the ground. In the first terminal run `roscore`:

```bash
roscore
```

In another terminal, launch webots:

```bash
# Go back to the root folder of the webots-simulation repository
cd ../../..
webots worlds/krock-2-obstacles.wbt
```

At the moment, the simulation is commanded by inputs coming from a Dualshock 4 controller (i.e. if krock doesn't move, this is why). 
The program will look for it under `/dev/input/js0`. 

## Debugging the controller on Webots

When the controller crashes in Webots, the error messages will not be broadcasted
inside the Webots console. This makes the debugging process tedious as it is then
necessary to add a lot of prints to find out where the crash is occuring and how
to solve the issue.

It is possible to run the gdb debugger with the Webots controller as follows:
1. In webots, change the robot controller to `<extern>`
2. Start the simulation.
3. Compile the controller with the debug symbols:
`cmake .. -DFOR_WEBOTS=ON -DWITH_ROS=ON -DCMAKE_BUILD_TYPE=Debug`
4. You need to export the path to Webots libraries to your environment:
(For Linux) `export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/webots/lib/controller`
5. Run the debugger: `gdb krock-controller`.
6. Use gdb as you please ([Quick guide](https://www.tutorialspoint.com/gnu_debugger/gdb_quick_guide.htm))

## Calling Krock2 in a designated world

If it's needed to bring `krock2` in a new `Webots` world in which the robot is not embedded, follow the instructions below:

1. Create the desired obstacles/patches which can include `Fluid` nodes
2. Right-click on the `Webots` tree and select `Add New`. Then, click on `Import` at the bottom right of the opened window.
3. Select [`MX64.wbo`](https://gitlab.com/biorob-krock/webots-simulation/-/blob/master/worlds/MX64.wbo) which is a required `Transform` by the robot
4. Repeat step `2`
5. This time click on the [`Swimmer_Krock.wbo`](https://gitlab.com/biorob-krock/webots-simulation/-/blob/master/worlds/Swimmer_Krock.wbo) file and bring the robot to the desired world file
6. One can save the world file and `Krock` will be embedded in this world file from that point onwards
